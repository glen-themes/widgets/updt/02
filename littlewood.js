$(document).ready(function(){
    // ht presents: htmlCeption
    fetch("//gt-littlewood-tab.glitch.me/glenjafishtml.html")
    .then(data => {
      return data.text()
    })
    .then(data => {
      $(".glenjafish").html(data)
    });
    
    img_t = setInterval(function(){
      if($(".icon-image").length > 0){
          var imgsrc = $("img").filter("[tab-data]").attr("src");
          $(".icon-image").attr("src", imgsrc);
          clearInterval(img_t);
      }
    },0);
    
    txt_t = setInterval(function(){
      if($(".chocolat").length > 0){
          var badgetxt = $("span").filter("[tab-data]").html().trim();
          $(".chocolat").html(badgetxt);
          clearInterval(txt_t);
      }
    },0);
});// end ready
